import Carousel from './components/Carousel'

document.addEventListener( 'DOMContentLoaded', function () {
  let carousel = new Carousel( {
    controls: true,
    element: '.carousel',
    pagination: true,
    reverse: false,
    slides: '.slide',
    speed: 5000,
    transition: 'fade'
  } )
  carousel.initialize()
} )
