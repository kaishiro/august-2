export default class Carousel {
  //
  constructor ( {
    controls = false,
    element = '#carousel',
    pagination = false,
    reverse = false,
    slides = '.slide',
    speed = 5000,
    transition = 'fade'
  } ) {
    this.carousel = document.querySelector( element )
    this.slides = document.querySelectorAll( slides )
    this.transition = transition
    this.speed = speed
    this.controls = controls
    this.reverse = reverse
    this.current = 0
    this.pagination = pagination
    this.pagers = []
  }
  activate ( index ) {
    this.slides[ index ].classList.add( 'is--active' )
    this.slides[ index ].setAttribute( 'aria-hidden', false )
    this.current = index

    if ( this.pagination ) {
      this.pagers[ index ].classList.add( 'is--active' )
    }
  }
  deactivate ( index ) {
    this.slides[ index ].classList.remove( 'is--active' )
    this.slides[ index ].setAttribute( 'aria-hidden', true )

    if ( this.pagination ) {
      this.pagers[ index ].classList.remove( 'is--active' )
    }
  }
  next () {
    // Deactivate current slide
    this.deactivate( this.current )

    if ( this.current === ( this.slides.length - 1 ) ) {
      this.activate( 0 )
    } else {
      this.current++
      this.activate( this.current )
    }
  }
  previous () {
    // Deactivate current slide
    this.deactivate( this.current )

    // Activate new slide or loop around
    if ( this.current === 0 ) {
      this.activate( ( this.slides.length - 1 ) )
    } else {
      this.current--
      this.activate( this.current )
    }
  }
  start ( reverse ) {
    setInterval( () => {
      // Which way do we want to go?
      reverse ? this.previous() : this.next()
    }, this.speed )
  }
  initializeControls () {
    let next = document.createElement( 'button' )
    next.setAttribute( 'aria-label', 'Next Slide' )
    next.className = 'carousel__button carousel__button--next'
    next.addEventListener( 'click', ( event ) => {
      this.next()
    } )
    this.carousel.appendChild( next )

    let previous = document.createElement( 'button' )
    previous.setAttribute( 'aria-label', 'Previous Slide' )
    previous.className = 'carousel__button carousel__button--previous'
    previous.addEventListener( 'click', ( event ) => {
      this.previous()
    } )
    this.carousel.appendChild( previous )
  }
  initializePagination () {
    // Create container
    let pagination = document.createElement( 'div' )
    pagination.className = 'carousel__pagination';

    // Create child pagers
    [ ...this.slides ].forEach( ( slide, index ) => {
      let pager = document.createElement( 'button' )
      pager.setAttribute( 'aria-label', `Slide #${index}` )
      index === 0 ? pager.className = 'carousel__pager is--active' : pager.className = 'carousel__pager'
      pager.addEventListener( 'click', ( event ) => {
        this.deactivate( this.current )
        this.activate( index )
      } )
      this.pagers.push( pager )
      pagination.appendChild( pager )
    } )

    this.carousel.appendChild( pagination )
  }
  initialize () {
    console.log( 'Initialize Carousel', this.active, this.yolo )

    // Set transition type
    this.carousel.setAttribute( 'data-transition', this.transition )

    // If there is only one slide than don't bother
    if ( this.slides.length > 1 ) {
      // Controls?
      if ( this.controls ) { this.initializeControls() }

      // Pagination?
      if ( this.pagination ) { this.initializePagination() }

      // Set initial slide
      this.activate( 0 )

      // Start the show
      this.start( this.reverse )
    }
  }
}
