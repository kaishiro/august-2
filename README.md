# August - Activity two.

### A live example of this project can be found at: https://august-2.netlify.com/

This task was interesting - largely because I would never create a carousel on my own anymore - I generally defer to something like Slick.js.  In terms of the project specs, I've done a simple recreation of the current August carousel.  In a real world application I would most likely roll in a framework, but for this exercise I've just used vanilla JS.  There is a single Carousel class which accepts a settings object on construction.  Current settings include:

| Setting | Description | Default |
|---|---|---|
| `controls` | Display the Carousel navigation controls | `false` |
| `element` | Selector to initialize the Carousel on | `#carousel` |
| `pagination` | Display the Carousel pagination controls | `false` |
| `reverse` | Play Carousel in reverse | `false` (Because this is a ridiculous option) |
| `slides` | Selector to target for the Carousel slides | `.slide` |
| `speed` | Speed at which the slides will transition (in milliseconds) | `5000` |
| `transition` | Type of transition between slides | `fade` |


## Setup

This project uses [Parcel](https://parceljs.org/) for local development and production builds. It's spec'd as a dev dependency so doesn't need to be installed globally. The following should be enough to get the project up and running locally.

```javascript
// Install dependencies
npm i

// Run locally
npm run develop
```
